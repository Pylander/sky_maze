MyGame.graphics = (function(){
    let canvas = document.getElementById('canvas-main');
    let context = canvas.getContext('2d');

    function clear(){
        context.clearRect(0, 0, canvas.width, canvas.height);
    };

    function setFill(x,y){
        //context.fillStyle = 'rgba(' +  + ',0,0,1)';
    };

    function Rectangle(spec){
        let that = {
            x: spec.x
        };

        that.updateRotation = function(angle){
            spec.rotation += angle;
        };

        that.draw = function(){
            context.save();
            context.translate(that.x + spec.width / 2, spec.y + spec.height / 2);
            context.rotate(spec.rotation);
            context.translate(-(spec.x + spec.width / 2), -(spec.y + spec.height / 2));

            //context.fillStyle = spec.fillStyle;
            context.fillRect(spec.x, spec.y, spec.width, spec.height);

            context.strokeStyle = spec.strokeStyle;
            context.strokeRect(spec.x, spec.y, spec.width, spec.height);

            context.restore();
        };

        return that;
    };

    function assignColor(terrainVal){
        var color;
        var i1 = .55;
        var i2 = .6;
        if(terrainVal<i1){
            color = "rgba(0,0,"+((terrainVal*255)+30)+",1)";
        } else if (terrainVal>=i1 && terrainVal<i2){
            color = "rgba("+(terrainVal*255)+","+(terrainVal*255)+"0,1)";
        } else {
            color = "rgba(0,"+(terrainVal*255-80)+",0,1)";
        }

        return color;
    };

    function drawBG(specs){
        for(var canvas_x=0; canvas_x < specs.canvasX * specs.canvasScalar; canvas_x++){
            for(var canvas_y=0; canvas_y < specs.canvasY * specs.canvasScalar; canvas_y++){
                var terrainVal = MyGame.noiseGen.getVal((specs.x + canvas_x) * specs.noiseScalar, 
                    (specs.y + canvas_y) * specs.noiseScalar);
                context.fillStyle = assignColor(terrainVal);
                context.fillRect(canvas_x*specs.sizeScalar,canvas_y*specs.sizeScalar,specs.sizeScalar,specs.sizeScalar);
            }
        }
    };

    //If you use allmand braces the compiler will put a ; after the return and you'll lose the object
    return {
        clear: clear,
        Rectangle: Rectangle,
        drawBG: drawBG,
        setFill: setFill
    };
})();
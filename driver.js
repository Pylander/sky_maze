MyGame.main = (function(graphics){ //Now you can just write graphics.*
    let previousTime;
    let BGspecs = {
        x: 0,
        y: 0,
        canvasX: 30,
        canvasY: 20,
        BGTimeInterval: 100, //ms
        BGTime: 0,
        canvasScalar: 10,
        sizeScalar: 1,
        noiseScalar: .035
    }

    let initialize = function(){
        previousTime = performance.now();
    };

    function update(elapsedtime){
        BGspecs.BGTime += elapsedtime;
        
        if(BGspecs.BGTime > BGspecs.BGTimeInterval){
            BGspecs.BGTime = 0;
            BGspecs.x+=2;
            BGspecs.y+=1;
        }
    };

    function render(){
        graphics.clear();
        graphics.drawBG(BGspecs);
    };

    function gameLoop(currentTime){
        var elapsedtime = currentTime - previousTime;
        previousTime = currentTime;

        update(elapsedtime);
        render(elapsedtime);

        requestAnimationFrame(gameLoop);
    };

    initialize();
    requestAnimationFrame(gameLoop);

})(MyGame.graphics);